# NonChan

## Dependencies (Ubuntu 13.10)

Most dependencies are resolved through composer, but a few will need to be installed manually.

### v8js

Install the required aptitude packages.

    sudo apt-get install libv8-dev libv8-dbg g++ cpp php5-dev

Compile the v8js module.

    sudo pecl install v8js-0.1.3

Create the file "/etc/php5/mods-available/v8js.ini" containing the following.

    ; configuration for php v8js module
    ; priority=20
    extension=v8js.so

Enable the PHP module.

    sudo php5enmod -s ALL v8js

Restart Apache.
   
    sudo service apache2 restart

## Services

[TO-DO]

## Twig Extensions

[TO-DO]