<?php

namespace Nonchan\CoreBundle\AssetInstaller;

use Symfony\Component\Finder\Finder;

class Installer{
    
    const REGEX_WILDCARD_PATH = '/^(.+)\/\*$/';
    
    private $map;
    
    public function __construct(){
        $this->map = array();
    }
    
    public function appendMap(array $map){
        $this->map = array_merge($this->map, $map);
    }
    
    public function getMap(){
        return $this->map;
    }
    
    private function installAsset($srcPath, $dstPath){
        
        $wildcardMatch = null;
        if(preg_match(self::REGEX_WILDCARD_PATH, $srcPath, $wildcardMatch)){
            
            $rawSrcDir = $wildcardMatch[1];
            
            if(!file_exists($rawSrcDir)){
                throw new \RuntimeException('Source directory does not exist');
            }
            $srcDir = realpath($rawSrcDir);
            
            $srcDirRegex = sprintf('/^%s\//', preg_quote($srcDir, '/'));
            
            $finder = new Finder();
            $finder->files()->in($srcDir);
            
            foreach($finder as $srcFile){
                
                $srcFilePath = (string)$srcFile;
                
                $relPath = preg_replace($srcDirRegex, '', $srcFilePath);
                $dstFilePath = sprintf('%s/%s', $dstPath, $relPath);
                
                $this->installAsset($srcFilePath, $dstFilePath);
            }
            
        }
        else{
            
            if(!file_exists($srcPath)){
                throw new \RuntimeException(sprintf('Source file %s does not exist', $srcPath));
            }
            
            $dstDirPath = dirname($dstPath);
            if(!file_exists($dstDirPath)){
                mkdir($dstDirPath, 0777, true);
            }
            
            $realDstPath = sprintf('%s/%s', realpath($dstDirPath), basename($dstPath));
            
            if(file_exists($realDstPath)) unlink($realDstPath);
            symlink($srcPath, $realDstPath);
            
        }
        
    }
    
    public function installAssets(){
        
        foreach($this->map as $mapping){
            $this->installAsset($mapping['source'], $mapping['destination']);
        }
        
    }
    
}