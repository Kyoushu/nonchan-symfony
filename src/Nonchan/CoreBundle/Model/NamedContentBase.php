<?php

namespace Nonchan\CoreBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Nonchan\CoreBundle\Form\Mapping as Form;
use Nonchan\CoreBundle\Table\Mapping as Table;
use JMS\Serializer\Annotation as JMS;

/**
 * ContentBase
 *
 * @ORM\MappedSuperclass
 * @JMS\ExclusionPolicy("all")
 */
class NamedContentBase extends ContentBase
{

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     * 
     * @Assert\NotBlank
     * 
     * @Form\Field(type="text", weight=-50)
     * @Table\Column
     */
    protected $name;
    
     /**
      * @var string
      * @Gedmo\Slug(fields={"name"}, updatable=false)
      * @ORM\Column(name="slug", type="string", length=255)
      * 
      * @JMS\Expose
      * @JMS\Type("string")
      * @JMS\Groups({"crud.publisher", "crud.webservice"})
      * 
      * @Form\Field(type="text", roles={"ROLE_ADMIN"}, weight=-49)
      *
     */
    protected $slug;
    
    /**
     * Set name
     *
     * @param string $name
     * @return ContentBase
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return ContentBase
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

}
