<?php

namespace Nonchan\CoreBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Nonchan\CoreBundle\Entity\User;
use Nonchan\CoreBundle\Form\Mapping as Form;
use Nonchan\CoreBundle\Table\Mapping as Table;
use JMS\Serializer\Annotation as JMS;

/**
 * ContentBase
 *
 * @ORM\MappedSuperclass
 * @JMS\ExclusionPolicy("all")
 */
class ContentBase
{

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * 
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     * 
     * @Table\Column()
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     * 
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     * 
     * @Table\Column()
     */
    protected $updated;

    /**
     * @var \Nonchan\CoreBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * @Table\Column(property="username")
     * 
     * @JMS\Expose
     * @JMS\Type("Nonchan\CoreBundle\Entity\User")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     * 
     * @Form\Field(
     *      type="entity",
     *      options={"class"="NonchanCoreBundle:User"},
     *      roles={"ROLE_ADMIN"},
     *      weight=99
     * )
     */
    protected $author;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="private", type="boolean", nullable=true)
     * @Form\Field(type="checkbox", weight=100)
     * @Table\Column()
     */
    protected $private;
    
    public function __construct(){
        $this->private = false;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ContentBase
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ContentBase
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set author
     *
     * @param \Nonchan\CoreBundle\Entity\User $author
     * @return ContentBase
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Nonchan\CoreBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }
    
    public function getAuthorId(){
        if(!$this->getAuthor()) return null;
        return $this->getAuthor()->getId();
    }
    
    /**
     * Get private
     * 
     * @return boolean
     */
    public function getPrivate()
    {
        return (bool)$this->private;
    }
    
    /**
     * Set private
     * @param boolean $private
     * @return \Nonchan\CoreBundle\Model\ContentBase
     */
    public function setPrivate($private)
    {
        $this->private = $private;
        return $this;
    }
    
    /**
     * Is private
     * 
     * @return boolean
     */
    public function isPrivate(){
        return $this->getPrivate();
    }
    
}
