(function(){
    
    var nonchan = (typeof window.nonchan === 'undefined' ? {ext: {}} : window.nonchan);
    
    nonchan.ext.view = (function(){
        
        var views = {};
        
        function viewExists(viewName){
            return (typeof views[viewName] !== 'undefined');
        }
        
        function createView(viewName, context){
            if(!viewExists(viewName)){
                throw 'nonchan.ext.view: view ' + viewName + ' undefined';
                return;
            }
            
            var view = views[viewName].prototypeElement.clone();
            
            view.updateContext = function(context){
                views[viewName].contextUpdater(this, context);
            };
            
            view.updateContext(context);
            
            return view;
            
        }
        
        function defineView(viewName, prototypeElement, contextUpdater){
            views[viewName] = {
                prototypeElement: prototypeElement,
                contextUpdater: contextUpdater
            };
            nonchan.ext.logger.log('nonchan.ext.view', 'view ' + viewName + ' defined');
        }
        
        function init(){
            
            nonchan.ext.view.defineView(
                    
                'nonchan.board.post',
        
                $('<div>').addClass('board-post').append(
                    $('<div>').addClass('row').append(
                        $('<div>').addClass('large-12 columns author').text('User Name')
                    )
                ),
        
                function(view, context){
                    view.find('.author:first').text(context.author.username);
                }
                        
            );
            
        }
        
        return {
            init: init,
            createView: createView,
            defineView: defineView,
            views: views
        };
        
    })();
    
})();