(function(){
    
    var nonchan = (typeof window.nonchan === 'undefined' ? {ext: {}} : window.nonchan);
    
    nonchan.ext.subscribe = (function(){
        
        var listeners = [];
        
        var defaultPollInterval = 5000;
        var webserviceRoute = 'nonchan_webservice_crud_publisher';

        // Listener Prototype
        
        var listener = function(startDatetime, expressionFilter, callback, pollInterval){
            
            if(typeof startDatetime === 'undefined') throw 'startDatetime required';
            if(typeof callback !== 'function') throw 'callback must be a function';
            
            this.lastDatetime = startDatetime;
            this.expressionFilter = (typeof expressionFilter === 'undefined' ? null : expressionFilter);
            this.callback = callback;
            this.pollInterval = (typeof pollInterval === 'undefined' ? defaultPollInterval : pollInterval);
            
            this.start();
        };
        
        listener.prototype.lastDatetime = null;
        listener.prototype.expressionFilter = null;
        listener.prototype.pollInterval = null;
        listener.prototype.pollTimeout = null;
        listener.prototype.callback = null;
        
        listener.prototype.start = function(){
            nonchan.ext.logger.log('nonchan.ext.subscribe', 'starting listener with expression filter "' + this.expressionFilter + '"');
            this.poll();
        };
        
        listener.prototype.stop = function(){
            nonchan.ext.logger.log('nonchan.ext.subscribe', 'stopping listener');
            clearTimeout(this.pollTimeout);
        };
        
        listener.prototype.poll = function(){
            var listenerScope = this;
            request(listenerScope.lastDatetime, listenerScope.expressionFilter, function(response){
                
                listenerScope.lastDatetime = response.datetime;
                
                nonchan.ext.logger.log('nonchan.ext.subscribe', 'polling again in ' + listenerScope.pollInterval + 'ms');
                listenerScope.pollTimeout = setTimeout(function(){
                    listenerScope.poll.call(listenerScope);
                }, listenerScope.pollInterval);
                
                listenerScope.callback(response);
                
            });
        };
        
        // Extension methods
        
        function request(afterDatetime, expressionFilter, callback){
            
            if(typeof afterDatetime === 'undefined') throw 'afterDatetime is required';
            if(typeof callback !== 'function') throw 'callback must be a function';
            
            expressionFilter = (typeof expressionFilter === 'undefined' ? null : expressionFilter);
            
            var url = nonchan.ext.routing.generate(webserviceRoute, {
               afterDatetime: afterDatetime,
               expressionFilter: expressionFilter
            });
            
            nonchan.ext.logger.log('nonchan.ext.subscribe', 'request "' + url + '"');
            
            $.get(url, null, function(response){
                if(response.status !== 'ok'){
                    throw 'nonchan.ext.subscribe: ' + response.reason;
                    return;
                }
                callback(response);
            });
            
        }
        
        function addListener(startDatetime, expressionFilter, callback, pollInterval){
            var newListener = new listener(startDatetime, expressionFilter, callback, pollInterval);
            listeners.push(newListener);
            return newListener;
        }
        
        return {
            request: request,
            addListener: addListener
        };
        
    })();
    
})();