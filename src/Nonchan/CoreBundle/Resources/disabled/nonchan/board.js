(function(){
    
    var nonchan = (typeof window.nonchan === 'undefined' ? {ext: {}} : window.nonchan);
    
    nonchan.ext.board = (function(){
        
        var postClass = 'Nonchan\\\\CoreBundle\\\\Entity\\\\Post';
        
        var board = function(container, startDatetime){
            
            nonchan.ext.logger.log('nonchan.ext.board', 'board created');
            
            var boardScope = this;
            
            if(typeof startDatetime !== 'undefined') this.startDatetime = startDatetime;
            
            this.container = container;
            
            this.subscribeListener = nonchan.ext.subscribe.addListener(this.startDatetime, this.expressionFilter, function(response){
                boardScope.handleResponse(response);
                boardScope.subscribeListener.stop();
            });
            
        };
        
        board.prototype.startDatetime = '24 hours ago';
        board.prototype.expressionFilter = 'type in ["create", "update", "delete"] and class == "' + postClass + '"';
        board.prototype.container = null;
        board.prototype.subscribeListener = null;
        
        board.prototype.postContainers = {};
        board.prototype.postContainerExists = function(post){
            return (typeof this.postContainers[post.id] !== 'undefined');
        };
        board.prototype.getPostContainer = function(post){
            if(!this.postContainerExists(post)) return false;
            return this.postContainers[post.id];
        };
        board.prototype.removePostContainer = function(post){
            if(!this.postContainerExists(post)) return false;
            this.postContainers[post.id].remove();
            delete this.postContainers[post.id];
        };
        board.prototype.createPostContainer = function(post){
            var postContainer = nonchan.ext.view.createView('nonchan.board.post', post);
            this.postContainers[post.id] = postContainer;
            this.container.append(postContainer);
            return postContainer;
        };
        board.prototype.updatePostContainer = function(post){
            if(!this.postContainerExists(post)) return false;
            this.postContainers[post.id].updateContext(post);
        };
        
        board.prototype.handleResponse = function(response){
            var boardScope = this;
            $.each(response.data.result, function(index, item){
                
                var event = item.event;
                var post = event.entity;
                
                if(event.type === 'create'){
                    nonchan.ext.logger.log('nonchan.ext.board', 'creating post container');
                    boardScope.createPostContainer(post);
                }
                else if(event.type === 'update'){
                    nonchan.ext.logger.log('nonchan.ext.board', 'updating post container');
                    boardScope.updatePostContainer(post);
                }
                else if(event.type === 'delete'){
                    nonchan.ext.logger.log('nonchan.ext.board', 'removing post container');
                    boardScope.removePostContainer(post);
                }
                
            });
        };
        
        function createBoard(container){
            var instance = new board(container);
            return instance;
        }
        
        return {
            createBoard: createBoard,
        };
        
    })();
    
})();