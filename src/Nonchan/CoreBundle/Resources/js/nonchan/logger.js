define('nonchan/logger', [], function(){
    
    var enabled = true;
    var messagePrefix = 'nonchan/logger: ';
    
    function enable(){
        enabled = true;
    };

    function disable(){
        enabled = false;
    };
    
    return {
        "enable": enable,
        "disable": disable,
        "log": function(message){
            if(!enabled) return;
            if(typeof console === 'undefined'){
                print(messagePrefix + message);
            }
            else{
                console.log(messagePrefix + message);
            }
        }
    };
    
});