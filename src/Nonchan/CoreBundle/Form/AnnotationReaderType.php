<?php

namespace Nonchan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Nonchan\CoreBundle\Form\Mapping as Form;
use Symfony\Component\Security\Core\SecurityContext;

class AnnotationReaderType extends AbstractType
{
    
    private $securityContext;
    
    public function __construct(SecurityContext $securityContext){
        
        $this->securityContext = $securityContext;
        
    }
    
    public function buildFormFromReflectionClass(\ReflectionClass $classRef, FormBuilderInterface $builder, array $options){
        
        $userRoles = array('ROLE_USER');
        if($this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->securityContext->getToken()->getUser();
            $userRoles = $user->getRoles();
        }
        
        $reader = new AnnotationReader();
        
        $fieldDefinitions = array();
        
        foreach($classRef->getProperties() as $propertyRef){
            
            $name = $propertyRef->getName();
            
            foreach($reader->getPropertyAnnotations($propertyRef) as $annotation){
                if(!$annotation instanceof Form\Field) continue;
                
                $userHasRole = false;
                foreach($annotation->roles as $annotationRole){
                    if(in_array($annotationRole, $userRoles)){
                        $userHasRole = true;
                        break;
                    }
                }
                
                if(!$userHasRole) continue;
                
                $fieldDefinitions[] = array(
                    'annotation' => $annotation,
                    'name' => $name
                );
                
            }
            
        }
        
        usort($fieldDefinitions, function($a, $b){
            if($a['annotation']->weight === $b['annotation']->weight) return 0;
            return ($a['annotation']->weight > $b['annotation']->weight ? 1 : -1);
        });
        
        foreach($fieldDefinitions as $fieldDefinition){
            $builder->add(
                $fieldDefinition['name'],
                $fieldDefinition['annotation']->type,
                $fieldDefinition['annotation']->options
            );
        }
        
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $classRef = new \ReflectionClass($options['data_class']);
        $this->buildFormFromReflectionClass($classRef, $builder, $options);
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        $resolver->setRequired(array('data_class'));
        
    }
    
    public function getName() {
        return 'annotation_reader';
    }
    
}