<?php

namespace Nonchan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CRUDType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('submit', 'submit', array(
            'label' => ucfirst(strtolower($options['crud_action']))
        ));
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        $resolver->setRequired(array(
            'data_class',
            'crud_action'
        ));
        
    }
    
    /**
     * @return string
     */
    public function getParent(){
        return 'annotation_reader';
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return 'content_base_crud';
    }
}
