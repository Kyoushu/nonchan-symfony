<?php

namespace Nonchan\CoreBundle\Form\Mapping;

/**
 * @Annotation
 */
class Field
{
    
    /**
     * @var string
     */
    public $type;

    /**
     * @var array
     */
    public $options = array();
    
    /**
     *
     * @var array
     */
    public $roles = array('ROLE_USER');
    
    /**
     *
     * @var integer
     */
    public $weight = 0;

}
