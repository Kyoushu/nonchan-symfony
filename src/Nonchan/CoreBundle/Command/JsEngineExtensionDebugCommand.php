<?php

namespace Nonchan\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class JsEngineExtensionDebugCommand extends ContainerAwareCommand{
    
    protected function configure(){
        
        $this
            ->setName('nonchan:js-engine:ext-debug')
            ->setDescription('Output lines from extension scripts for debugging purposes')
            ->addArgument(
                'extension',
                InputArgument::REQUIRED,
                'Extension name'
            )
            ->addArgument(
                'start-line-number',
                InputArgument::OPTIONAL,
                'Start line number',
                null
            )
            ->addArgument(
                'end-line-number',
                InputArgument::OPTIONAL,
                'End line number (returns single line if omitted)',
                null
            )
        ;
        
    }
    
    protected function execute(InputInterface $input, OutputInterface $output){
        
        /* @var $engine \Nonchan\CoreBundle\JsEngine\Factory */
        $factory = $this->getContainer()->get('nonchan.js_engine.factory');
        
        $name = $input->getArgument('extension');
        
        $startLine = $input->getArgument('start-line-number');
        
        if($startLine !== null){
            
            $endLine = (
                $input->getArgument('end-line-number') ?
                $input->getArgument('end-line-number') :
                $startLine
            );
            
            if(!is_numeric($startLine)) throw new \RuntimeException('start-line-number must be an integer');
            if(!is_numeric($endLine)) throw new \RuntimeException('end-line-number must be an integer');

            $offset = $startLine - 1;
            $length = $endLine - $startLine + 1;

            $script = explode("\n", $factory->getScript($name));     
            $output->writeln(implode("\n", array_slice($script, $offset, $length)));
            
        }
        else{
            $script = $factory->getScript($name);
            $output->writeln( $script );
        }
        
    }
}
