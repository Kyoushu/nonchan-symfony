<?php

namespace Nonchan\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class JsEngineScriptCommand extends ContainerAwareCommand{
    
    protected function configure(){
        
        $this
            ->setName('nonchan:js-engine:script')
            ->setDescription('Output print() buffer from evaluated script')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Script path'
            )
            ->addOption(
                'deps',
                null,
                InputOption::VALUE_REQUIRED,
                'Dependencies (comma separated extension names)',
                ''
            )
        ;
        
    }
    
    protected function execute(InputInterface $input, OutputInterface $output){
        
        /* @var $engine \Nonchan\CoreBundle\JsEngine\Factory */
        $factory = $this->getContainer()->get('nonchan.js_engine.factory');
        
        $inputDependencies = explode(',', $input->getOption('deps'));
        array_walk($inputDependencies, function(&$name){
           $name = trim($name); 
        });
        $dependencies = array_filter($inputDependencies);
        
        $path = $input->getArgument('path');
        if(!file_exists($path)){
            throw new \RuntimeException('Script does not exist');
        }
        
        $output->writeln('');
        $output->writeln(sprintf(
            '<info>Executing script%s</info>',
            (
                count($dependencies) > 0 ?
                ' with deps "' . implode('", "', $dependencies) . '"' :
                ' without deps'
            )
        ));
        
        $script = file_get_contents($path);
        $engine = $factory->createEngine('debug', array(), $dependencies);
        
        $result = $engine->executeScript($script);
                
        $output->writeln('<info>Output start</info>');
        $output->writeln($result);
        $output->writeln('<info>Output end</info>');
        $output->writeln('');
        
    }
}
