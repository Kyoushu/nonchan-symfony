<?php

namespace Nonchan\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class JsEngineTemplatingCommand extends ContainerAwareCommand{
    
    protected function configure(){
        
        $this
            ->setName('nonchan:js-engine:templating')
            ->setDescription('Generate HTML using V8js and nonchan.view.js')
            /*->addArgument(
                'script',
                InputArgument::REQUIRED,
                'Javascript to execute'
            )*/
        ;
        
    }
    
    protected function execute(InputInterface $input, OutputInterface $output){
        
        $templating = $this->getContainer()
                ->get('nonchan.js_templating');
        
        
        
    }
}
