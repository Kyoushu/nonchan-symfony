<?php

namespace Nonchan\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallAssetsCommand extends ContainerAwareCommand{
    
    protected function configure(){
        $this
            ->setName('nonchan:install-assets')
            ->setDescription('Install CSS and JS assets')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output){
        $output->writeln('Installing Nonchan Assets');
        $installer = $this->getContainer()->get('nonchan.asset_installer');
        $installer->installAssets();
    }
    
}
