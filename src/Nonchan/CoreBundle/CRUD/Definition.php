<?php

namespace Nonchan\CoreBundle\CRUD;

class Definition{
    
    private $name;
    private $namePlural;
    private $entityClass;
    private $createRoles;
    
    static function pluralise($name){
        return sprintf('$ss', $name);
    }
    
    public function __construct($name, $entityClass){
        
        $this->name = $name;
        $this->namePlural = self::pluralise($name);
        $this->entityClass = $entityClass;
        $this->createRoles = array();
                
    }
    
    /**
     * Get entityClass
     * @return string
     */
    public function getEntityClass(){
        return $this->entityClass;
    }
    
    /**
     * Creates a ReflectionClass object for the entity class
     * @return \ReflectionClass
     */
    public function createEntityReflection(){
        return new \ReflectionClass($this->entityClass);
    }
    
    /**
     * Get name
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    public function setNamePlural($namePlural){
        $this->namePlural = $namePlural;
        return $this;
    }
    
    public function getNamePlural(){
        return $this->namePlural;
    }
    
    public function setCreateRoles($createRoles){
        $this->createRoles = $createRoles;
        return $this;
    }
    
    public function getCreateRoles(){
        return $this->createRoles;
    }
    
}