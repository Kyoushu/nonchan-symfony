<?php

namespace Nonchan\CoreBundle\CRUD;

use Nonchan\CoreBundle\CRUD\Exception\UndefinedDefinitionException;

class Registry{
    
    private $definitions;
    private $config;
    
    public function __construct(){
        $this->definitions = array();
    }
    
    public function addDefinition(Definition $definition){
        $this->definitions[$definition->getName()] = $definition;
    }
    
    public function getDefinitions(){
        return $this->definitions;
    }
    
    public function getDefinition($name){
        if(!isset($this->definitions[$name])){
            throw new UndefinedDefinitionException(sprintf(
                'The definition %s could not be found',
                $name
            ));
        }
        return $this->definitions[$name];
    }
    
    public function removeDefinition($name){
        if(!isset($this->definitions[$name])){
            throw new UndefinedDefinitionException(sprintf(
                'The definition %s could not be found',
                $name
            ));
        }
        unset($this->definitions[$name]);
    }
    
    /**
     * Get definition for the specified entity
     * @param object $entity
     * @return boolean|\Nonchan\CoreBundle\CRUD\Definition
     */
    public function findDefinitionByEntity($entity){
        
        $class = get_class($entity);
        
        foreach($this->definitions as $definition){
            if($class === $definition->getEntityClass() || is_subclass_of($entity, $definition->getEntityClass())){
                return $definition;
            }
        }
        
        return false;
        
    }
    
    public function setConfig(array $config){
        $this->config = $config;
        
        foreach($config['definitions'] as $definitionConfig){
            
            $definition = new Definition($definitionConfig['name'], $definitionConfig['entity_class']);
            
            if($definitionConfig['name_plural']){
                $definition->setNamePlural($definitionConfig['name_plural']);
            }
            
            if($definitionConfig['create_roles']){
                $definition->setCreateRoles($definitionConfig['create_roles']);
            }
            
            $this->addDefinition($definition);
            
        }
        
        return $this;
    }
    
}