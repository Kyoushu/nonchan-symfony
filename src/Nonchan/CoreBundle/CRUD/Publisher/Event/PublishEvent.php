<?php

namespace Nonchan\CoreBundle\CRUD\Publisher\Event;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Serializer;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;

/**
 * @JMS\ExclusionPolicy("all")
 */
class PublishEvent{
    
    const TYPE_CREATE = 'create';
    const TYPE_UPDATE = 'update';
    const TYPE_DELETE = 'delete';
    
    /**
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     */
    protected $type;
    
    /**
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"crud.publisher"})
     */
    protected $entityJson;
    
    /**
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     */
    protected $class;
    
    /**
     * @JMS\Expose
     * @JMS\Groups({"crud.webservice"})
     */
    protected $entity;
    
    public function __construct($type = null, $entity = null){
        
        if($type !== null && $entity !== null){
        
            $this->type = $type;
            $this->entity = $entity;
            $this->entityJson = null;
            $this->class = get_class($entity);
            
        }
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function getEntityJson(){
        return $this->entityJson;
    }
    
    public function getEntity(){
        return $this->entity;
    }
    
    public function getClass(){
        return $this->class;
    }
    
    public function serializeEntity(Serializer $serializer, SerializationContext $context){
        $this->entityJson = $serializer->serialize($this->entity, 'json', $context);
    }
    
    public function deserializeEntity(Serializer $serializer, DeserializationContext $context){
        $this->entity = $serializer->deserialize($this->entityJson, $this->class, 'json', $context);
    }
    
}

