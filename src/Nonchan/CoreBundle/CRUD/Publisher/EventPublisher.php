<?php

namespace Nonchan\CoreBundle\CRUD\Publisher;

use Doctrine\ORM\EntityManager;
use Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent;
use Nonchan\CoreBundle\Entity\CRUDPublisherEvent;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializationContext;

class EventPublisher{
    
    private $entityManager;
    private $serializer;
    
    public function __construct(EntityManager $entityManager, Serializer $serializer){
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }
    
    public function dispatch(PublishEvent $event){
        
        $entity = new CRUDPublisherEvent();
        $entity->setEvent( $event );
        
        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);
        $this->entityManager->detach($entity);
        
    }
    
}
