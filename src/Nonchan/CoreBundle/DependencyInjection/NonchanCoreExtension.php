<?php

namespace Nonchan\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NonchanCoreExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        
        $crudRegistryServiceDefinition = $container->getDefinition('nonchan.crud.registry');
        $crudRegistryServiceDefinition->addMethodCall('setConfig', array( $config['crud'] ));
        
        $assetInstallerDefinition = $container->getDefinition('nonchan.asset_installer');
        $assetInstallerDefinition->addMethodCall('appendMap', array( $config['assets']['install_map'] ));
        
    }
}
