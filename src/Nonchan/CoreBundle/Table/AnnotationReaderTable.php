<?php

namespace Nonchan\CoreBundle\Table;

use Nonchan\CoreBundle\Table\Core\Table;
use Nonchan\CoreBundle\Table\Mapping as TableMapping;
use Doctrine\Common\Annotations\AnnotationReader;

class AnnotationReaderTable extends Table{
    
    private $class;
    
    public function __construct($class){
        $this->class = $class;
    }
    
    public function buildTable(){
        
        $classRef = new \ReflectionClass($this->class);
        $reader = new AnnotationReader();
        
        foreach($classRef->getProperties() as $propertyRef){
            
            foreach($reader->getPropertyAnnotations($propertyRef) as $annotation){
                
                if(!$annotation instanceof TableMapping\Column) continue;
                
                if($annotation->property){
                    $name = sprintf('%s.%s', $propertyRef->getName(), $annotation->property);
                }
                else{
                    $name = $propertyRef->getName();
                }
                
                $column = $this->addColumn($name);
                
                if($annotation->label){
                    $column->setLabel($annotation->label);
                }
                
            }
            
        }
        
    }
    
}
