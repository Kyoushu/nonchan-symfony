<?php

namespace Nonchan\CoreBundle\Table\Mapping;

/**
 * @Annotation
 */
class Column
{
    
    /**
     * @var string
     */
    public $property;
    
    /**
     * @var string
     */
    public $label;

}
