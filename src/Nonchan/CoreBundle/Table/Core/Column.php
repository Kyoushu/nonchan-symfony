<?php

namespace Nonchan\CoreBundle\Table\Core;

use Nonchan\CoreBundle\Table\Core\Table;
use Symfony\Component\PropertyAccess\PropertyAccess;

class Column{
    
    private $table;
    private $name;
    private $label;
    private $valueClosure;
    
    static function createPropertyValueClosure($propertyPath){
        
        return function($rowData) use ($propertyPath){
            
            try{
            
                $accessor = PropertyAccess::createPropertyAccessor();
                $value = $accessor->getValue($rowData, $propertyPath);
            
                if($value instanceof \DateTime){
                    return $value->format('D jS F Y, H:i');
                }
                elseif($value === true){
                    return 'Yes';
                }
                elseif($value === false){
                    return 'No';
                }
                else{
                    return (string)$value;
                }
            
            }
            catch(\Exception $e){
                return '#ERROR#';
            }
            
        };
        
    }
    
    public function __construct(Table $table, $name){
        $this->table = $table;
        $this->name = $name;
        $this->label = Table::humanize($name);
        $this->valueClosure = self::createPropertyValueClosure($name);
    }
    
    /**
     * Get name
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * Get label
     * @return string
     */
    public function getLabel(){
        return $this->label;
    }
    
    /**
     * Set label
     * @param string $label
     * @return \Nonchan\CoreBundle\Table\Column
     */
    public function setLabel($label){
        $this->label = $label;
        return $this;
    }
    
    public function setValueClosure(\Closure $valueClosure){
        $this->valueClosure = $valueClosure;
        return $this;
    }
    
    public function getValue($rowData){
        $valueClosure = $this->valueClosure;
        return $valueClosure($rowData);
    }
    
}