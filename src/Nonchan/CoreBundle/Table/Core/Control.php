<?php

namespace Nonchan\CoreBundle\Table\Core;

use Nonchan\CoreBundle\Table\Core\Table;

class Control{
    
    private $table;
    private $name;
    private $label;
    private $route;
    private $routeParametersClosure;
    private $visibilityClosure;
    
    public function __construct(Table $table, $name, $route = null){
        $this->table = $table;
        $this->name = $name;
        
        $this->label = Table::humanize($name);
        $this->route = $route;
        
        $this->routeParametersClosure = function(){
            return array();
        };
        
        $this->visibilityClosure = function(){
            return true;
        };
        
    }
    
    public function getRoute(){
        return $this->route;
    }
    
    public function setRoute($route){
        $this->route = $route;
        return $this;
    }
    
    public function setRouteParametersClosure(\Closure $routeParametersClosure){
        $this->routeParametersClosure = $routeParametersClosure;
    }
    
    public function getRouteParameters($rowData){
        $routeParametersClosure = $this->routeParametersClosure;
        return $routeParametersClosure($rowData);
    }
    
    public function isVisible($rowData){
        $visibilityClosure = $this->visibilityClosure;
        return $visibilityClosure($rowData);
    }
    
    public function setVisibilityClosure(\Closure $visibilityClosure){
        $this->visibilityClosure = $visibilityClosure;
        return $this;
    }
    
    /**
     * Get name
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * Get label
     * @return string
     */
    public function getLabel(){
        return $this->label;
    }
    
    /**
     * Set label
     * @param string $label
     * @return \Nonchan\CoreBundle\Table\Column
     */
    public function setLabel($label){
        $this->label = $label;
        return $this;
    }
    
}