<?php

namespace Nonchan\CoreBundle\Table\Core;

use Nonchan\CoreBundle\Table\Core\Column;
use Nonchan\CoreBundle\Table\Core\Control;
use Nonchan\CoreBundle\Table\Core\TableView;

use Nonchan\CoreBundle\Table\Exception\UndefinedColumnException;

class Table{
    
    private $columns;
    private $controls;
    private $data;
    
    static function humanize($string){
        return ucwords(strtolower(
            preg_replace(
                '/([a-z])([A-Z])/',
                '${1} ${2}',
                preg_replace(
                    '/\..+/',
                    '',
                    $string
                )
            )
        ));
    }
    
    public function __construct(){
        $this->columns = array();
        $this->controls = array();
        $this->data = array();
    }
    
    public function buildTable(){
        
    }
    
    public function getColumns(){
        return $this->columns;
    }
    
    public function getControls(){
        return $this->controls;
    }
    
    /**
     * 
     * @param string $name
     * @param string $route
     * @return \Nonchan\CoreBundle\Table\Core\Control
     */
    public function addControl($name, $route = null){
        $control = new Control($this, $name, $route);
        $this->controls[$name] = $control;
        return $control;
    }
    
    public function removeControl($name){
        if(!isset($this->controls[$name])){
            throw new UndefinedColumnException(sprintf('The table control %s could not be found', $name));
        }
        unset( $this->controls[$name] );
    }
    
    public function getControl($name){
        if(!isset($this->controls[$name])){
            throw new UndefinedColumnException(sprintf('The table control %s could not be found', $name));
        }
        return $this->controls[$name];
    }
    
    /**
     * Remove a column by name
     * @param string $name
     * @throws \Nonchan\CoreBundle\Table\Exception\UndefinedColumnException
     */
    public function removeColumn($name){
        if(!isset($this->columns[$name])){
            throw new UndefinedColumnException(sprintf('The table column %s could not be found', $name));
        }
        unset( $this->columns[$name] );
    }
    
    /**
     * Get a column by name
     * @param string $name
     * @return \Nonchan\CoreBundle\Table\Column;
     * @throws \Nonchan\CoreBundle\Table\Exception\UndefinedColumnException
     */
    public function getColumn($name){
        if(!isset($this->columns[$name])){
            throw new UndefinedColumnException(sprintf('The table column %s could not be found', $name));
        }
        return $this->columns[$name];
    }
    
    /**
     * Add columns
     * @param string $name
     * @return \Nonchan\CoreBundle\Table\Column
     */
    public function addColumn($name){
        $column = new Column($this, $name);
        $this->columns[$name] = $column;
        return $column;
    }
    
    public function setData(array $data){
        $this->data = $data;
    }
    
    public function getData(){
        return $this->data;
    }
    
    public function createView(){
        return new TableView($this);
    }
    
}