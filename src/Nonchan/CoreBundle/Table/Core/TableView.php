<?php

namespace Nonchan\CoreBundle\Table\Core;

class TableView{
    
    private $table;
    
    public function __construct(Table $table){
        $this->table = $table;
        $this->table->buildTable();
    }
    
    /**
     * Get columns
     * @return array
     */
    public function getColumns(){
        return $this->table->getColumns();
    }
    
    /**
     * Get controls
     * @return array
     */
    public function getControls(){
        return $this->table->getControls();
    }
    
    /**
     * Get data
     * @return array
     */
    public function getData(){
        return $this->table->getData();
    }
    
}
