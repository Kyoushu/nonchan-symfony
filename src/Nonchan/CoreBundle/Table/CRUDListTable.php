<?php

namespace Nonchan\CoreBundle\Table;

use Nonchan\CoreBundle\CRUD\Definition;
use Nonchan\CoreBundle\Table\AnnotationReaderTable;
use Symfony\Component\Security\Core\SecurityContext;

class CRUDListTable extends AnnotationReaderTable{
    
    private $definition;
    private $securityContext;
    
    public function __construct(Definition $definition, SecurityContext $securityContext){
        $this->definition = $definition;
        $this->securityContext = $securityContext;
        parent::__construct( $definition->getEntityClass() );
    }
    
    public function buildTable(){
        
        parent::buildTable();
        
        $definition = $this->definition;
        $securityContext = $this->securityContext;
        
        $this->addControl('read', 'nonchan_crud_read')
            ->setVisibilityClosure(function($rowData) use ($securityContext){
                return $securityContext->isGranted('read', $rowData);
            })
            ->setRouteParametersClosure(function($rowData) use ($definition){
                return array(
                    'name' => $definition->getName(),
                    'id' => $rowData->getId()
                );
            });
        
        $this->addControl('update', 'nonchan_crud_update')
            ->setVisibilityClosure(function($rowData) use ($securityContext){
                return $securityContext->isGranted('update', $rowData);
            })
            ->setRouteParametersClosure(function($rowData) use ($definition){
                return array(
                    'name' => $definition->getName(),
                    'id' => $rowData->getId()
                );
            });
            
        $this->addControl('delete', 'nonchan_crud_delete')
            ->setVisibilityClosure(function($rowData) use ($securityContext){
                return $securityContext->isGranted('delete', $rowData);
            })
            ->setRouteParametersClosure(function($rowData) use ($definition){
                return array(
                    'name' => $definition->getName(),
                    'id' => $rowData->getId()
                );
            });;
        
    }
    
}
