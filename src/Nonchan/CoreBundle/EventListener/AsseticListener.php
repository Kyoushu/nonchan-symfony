<?php

namespace Nonchan\CoreBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\Container;

class AsseticListener{
    
    private $container;
    
    public function __construct(Container $container){
        $this->container = $container;
    }
    
    public function onController(FilterControllerEvent $event){
        
        $controller = $event->getController();
        
        // Assetic doesn't understand how to cache SCSS properly, so we'll force
        // the site to re-generate CSS every time (if the assetic controller is
        // enabled in config_dev.yml)
        
        if(get_class($controller[0]) === 'Symfony\Bundle\AsseticBundle\Controller\AsseticController'){
            if($controller[1] === 'render'){
                $request = $event->getRequest();
                $uri = $request->getUri();
                if(preg_match('/\.css$/i', $uri)){
                    $event->setController(array(
                        $this->container->get('nonchan.assetic.controller'),
                        'render'
                    ));
                }
            }
        }
        
    }
    
}