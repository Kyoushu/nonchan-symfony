<?php

namespace Nonchan\CoreBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Nonchan\CoreBundle\Model\ContentBase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent;
use Nonchan\CoreBundle\Entity\CRUDPublisherEvent;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;

class EntityListener{
    
    private $container;
    private $securityContext;
    
    public function __construct(Container $container){
        $this->container = $container;
    }
    
    public function postPersist(LifecycleEventArgs $args){
        
        $entity = $args->getEntity();
        
        // Send change to publisher
        $registry = $this->container->get('nonchan.crud.registry');
        $definition = $registry->findDefinitionByEntity($entity);
        if($definition){
            $eventPublisher = $this->container->get('nonchan.crud.event_publisher');
            $event = new PublishEvent(PublishEvent::TYPE_CREATE, $entity);
            $eventPublisher->dispatch($event);
        }
        
    }
    
    public function postUpdate(LifecycleEventArgs $args){
        
        $entity = $args->getEntity();
        
        // Send change to publisher
        $registry = $this->container->get('nonchan.crud.registry');
        $definition = $registry->findDefinitionByEntity($entity);
        if($definition){
            $eventPublisher = $this->container->get('nonchan.crud.event_publisher');
            $event = new PublishEvent(PublishEvent::TYPE_UPDATE, $entity);
            $eventPublisher->dispatch($event);
        }
        
    }
    
    public function postRemove(LifecycleEventArgs $args){
        
        $entity = $args->getEntity();
        
        // Send change to publisher
        $registry = $this->container->get('nonchan.crud.registry');
        $definition = $registry->findDefinitionByEntity($entity);
        if($definition){
            $eventPublisher = $this->container->get('nonchan.crud.event_publisher');
            $event = new PublishEvent(PublishEvent::TYPE_DELETE, $entity);
            $eventPublisher->dispatch($event);
        }
        
    }
    
    public function postLoad(LifecycleEventArgs $args){
        
        $entity = $args->getEntity();
        
        if($entity instanceof CRUDPublisherEvent){
            
            try{
            
                $serializer = $this->container->get('jms_serializer');
            
                $context = new DeserializationContext();
                $context->setGroups(array('crud.publisher'));
            
                $entity->deserializeEvent($serializer, $context);
                
            }
            catch(\Exception $e){
                // Let deserialization fail silently
            }
        }
        
    }
    
    public function prePersist(LifecycleEventArgs $args){
        
        $entity = $args->getEntity();
        
        if($entity instanceof CRUDPublisherEvent){
            
            $serializer = $this->container->get('jms_serializer');
            
            $context = new SerializationContext();
            $context->setGroups(array('crud.publisher'));
            
            $entity->serializeEvent($serializer, $context);
            
        }
        elseif($entity instanceof ContentBase){
        
            // Load secutiy context here to resolve circular dependency issue
            try{
                $securityContext = $this->container->get('security.context');
                if(!$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) return;
                
                if($entity->getAuthor() === null){
                    $user = $this->securityContext->getToken()->getUser();
                    $entity->setAuthor($user);
                }    
            }
            catch(AuthenticationCredentialsNotFoundException $e){
                // Do nothing, the exception is most likely resulting from the script
                // being initiaded bia the CLI
            }
            
        }
        
    }
    
}