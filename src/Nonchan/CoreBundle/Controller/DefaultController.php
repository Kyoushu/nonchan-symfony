<?php

namespace Nonchan\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nonchan\CoreBundle\Form\BoardType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NonchanCoreBundle:Default:board.html.twig');
    }
}
