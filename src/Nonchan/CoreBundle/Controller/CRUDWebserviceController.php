<?php

namespace Nonchan\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CRUDWebserviceController extends Controller{
    
    const STATUS_OK = 'ok';
    const STATUS_EXCEPTION = 'exception';
    
    const PUBLISHER_RESULT_LIMIT = 1000;
    
    public function publisherAction($afterDatetime, $expressionFilter){
        
        $securityContext = $this->container->get('security.context');
        if(!$securityContext->isGranted('IS_AUTHENTICATED_FULLY')){
            throw new AccessDeniedException('You must be logged in to access webservices');
        }
        
        try{
            $afterDatetime = new \DateTime($afterDatetime);        
            
            $language = new ExpressionLanguage();
            
            $em = $this->getDoctrine()->getManager();
            
            $result = $em->getRepository('NonchanCoreBundle:CRUDPublisherEvent')
                ->createQueryBuilder('e')
                ->andWhere('e.created >= :afterDatetime')
                ->setParameter('afterDatetime', $afterDatetime)
                ->orderBy('e.created', 'ASC')
                ->setMaxResults(self::PUBLISHER_RESULT_LIMIT)
                ->getQuery()
                ->getResult();
            
            $filteredResult = array_filter($result, function($entity) use ($language, $expressionFilter, $securityContext){
                
                if(!$securityContext->isGranted('read', $entity->getEvent()->getEntity())){
                    return false;
                }
                
                return (bool)$language->evaluate(
                    $expressionFilter,
                    array(
                        'created' => $entity->getCreated(),
                        'type' => $entity->getEvent()->getType(),
                        'class' => $entity->getEvent()->getClass(),
                        'entity' => $entity->getEvent()->getEntity()
                    )
                );
            });
            
            return $this->createJsonDataResponse(array(
                'afterDatetime' => $afterDatetime->format('c'),
                'expressionFilter' => $expressionFilter,
                'result' => $filteredResult
            ));
            
        }
        catch(\Exception $e){           
            return $this->createJsonDataResponse(null, self::STATUS_EXCEPTION, $e->getMessage());
        }
        
    }
    
    public function createJsonDataResponse($data = null, $status = null, $reason = null){
        
        if($status === null) $status = self::STATUS_OK;
        $now = new \DateTime('now');
        
        $responseData = array(
            'status' => $status,
            'reason' => $reason,
            'datetime' => $now->format('c'),
            'data' => $data
        );
        
        $httpStatusCode = ($status === self::STATUS_EXCEPTION ? 500 : 200);
        
        $serializer = $this->container->get('jms_serializer');
        $context = new SerializationContext();
        $context->setGroups(array('crud.webservice'));
        $responseJson = $serializer->serialize($responseData, 'json', $context);
        
        $response = new Response($responseJson, $httpStatusCode, array(
            'Content-Type' => 'application/json'
        ));
        $response->setPrivate();
        
        return $response;
        
    }
    
}