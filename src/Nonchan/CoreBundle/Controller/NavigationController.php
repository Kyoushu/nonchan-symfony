<?php

namespace Nonchan\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NavigationController extends Controller{
    
    public function offCanvasStartAction(){
        
        $crudRegistry = $this->container->get('nonchan.crud.registry');
        
        return $this->render('NonchanCoreBundle:Navigation:off_canvas_start.html.twig', array(
            'crud_registry' => $crudRegistry
        ));
    }
    
    public function offCanvasEndAction(){
        return $this->render('NonchanCoreBundle:Navigation:off_canvas_end.html.twig');
    }
    
}
