<?php

namespace Nonchan\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nonchan\CoreBundle\Form\CRUDType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Nonchan\CoreBundle\Table\CRUDListTable;
use Nonchan\CoreBundle\CRUD\Definition;

class CRUDController extends Controller
{
    
    protected function currentUserCanCreate(Definition $definition){
        
        $securityContext = $this->container->get('security.context');
        if(!$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) return false;
        
        $user = $securityContext->getToken()->getUser();
        
        foreach($definition->getCreateRoles() as $createRole){
            if(in_array($createRole, $user->getRoles())){
                return true;
            }
        }
        
        return false;
        
    }
    
    public function readAction(){
        
        $response = new Response('No view available');
        $response->setPrivate();
        
        return $response;
        
    }
    
    public function listAction($name){
        
        $crudRegistry = $this->container->get('nonchan.crud.registry');
        $definition = $crudRegistry->getDefinition($name);
        
        $securityContext = $this->container->get('security.context');
        
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository( $definition->getEntityClass() )->findAll();
        
        $response = new Response();
        $response->setPrivate();
        
        $table = new CRUDListTable( $definition, $securityContext );
        $table->setData($entities);
        
        $context = array(
            'definition' => $definition,
            'entities' => $entities,
            'table' => $table->createView(),
            'can_create' => $this->currentUserCanCreate( $definition )
        );
        
        return $this->render('NonchanCoreBundle:CRUD:list.html.twig', $context, $response);
        
    }
    
    public function deleteAction(Request $request, $name, $id, $confirm){
        
        $crudRegistry = $this->container->get('nonchan.crud.registry');
        $definition = $crudRegistry->getDefinition($name);
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository( $definition->getEntityClass() )->find($id);
        
        if(!$entity){
            throw $this->createNotFoundException(sprintf('The specified %s entity could not be found', $definition->getName() ));
        }
        
        if(!$this->get('security.context')->isGranted('delete', $entity)){
            throw new AccessDeniedException(sprintf(
                'You don\'t have permission to delete the specified %s entity',
                $definition->getName()
            ));
        }
        
        $response = new Response();
        $response->setPrivate();
        
        if(!$confirm){
            
            $confirmUrl = $this->generateUrl('nonchan_crud_delete', array('name' => $name, 'id' => $id, 'confirm' => 'confirm'));
            $cancelUrl = $this->generateUrl('nonchan_crud_list', array('name' => $name));
            
            $context = array(
                'definition' => $definition,
                'confirm_url' => $confirmUrl,
                'cancel_url' => $cancelUrl
            );
            
            return $this->render('NonchanCoreBundle:CRUD:delete.html.twig', $context, $response);
            
        }
        
        $em->remove($entity);
        $em->flush();
        
        $redirectUrl = $this->generateUrl('nonchan_crud_list', array('name' => $name));
        
        $request->getSession()->getFlashBag()->add(
            'notice',
            sprintf('%s deleted', $definition->getName())
        );
        
        return $this->redirect($redirectUrl);
        
    }
    
    public function updateAction(Request $request, $name, $id){
        
        $crudRegistry = $this->container->get('nonchan.crud.registry');
        $definition = $crudRegistry->getDefinition($name);
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository( $definition->getEntityClass() )->find($id);
        
        if(!$entity){
            throw $this->createNotFoundException(sprintf('The specified %s entity could not be found', $definition->getName() ));
        }
        
        if(!$this->get('security.context')->isGranted('update', $entity)){
            throw new AccessDeniedException(sprintf(
                'You don\'t have permission to update the specified %s entity',
                $definition->getName()
            ));
        }
        
        $form = $this->createForm(new CRUDType(), $entity, array(
            'data_class' => $definition->getEntityClass(),
            'crud_action' => 'update' 
        ));
        
        if($request->isMethod('POST')){
            
            $form->handleRequest($request);
            if($form->isValid()){
                
                $em = $this->getDoctrine()->getManager();
            
                $em->persist($entity);
                $em->flush();
                
                $redirectUrl = $this->generateUrl('nonchan_crud_list', array(
                    'name' => $name
                ));
                
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    sprintf('%s updated', $definition->getName() )
                );
                
                return $this->redirect($redirectUrl);
                
            }
            
        }
        
        $response = new Response();
        $response->setPrivate();
        
        $context = array(
            'definition' => $definition,
            'form' => $form->createView()
        );
        
        return $this->render('NonchanCoreBundle:CRUD:update.html.twig', $context, $response);
        
    }
    
    public function createAction(Request $request, $name)
    {
        
        $crudRegistry = $this->container->get('nonchan.crud.registry');
        $definition = $crudRegistry->getDefinition($name);
        
        $class = $definition->getEntityClass();
        $entity = new $class;
        
        if(!$this->currentUserCanCreate( $definition )){
            throw new AccessDeniedException(sprintf(
                'You don\'t have permission to create %s entities',
                $definition->getName()
            ));
        }
        
        $form = $this->createForm(new CRUDType(), $entity, array(
            'data_class' => $class,
            'crud_action' => 'create'
        ));
        
        if($request->isMethod('POST')){
            
            $form->handleRequest($request);
            if($form->isValid()){
                
                $em = $this->getDoctrine()->getManager();
            
                $em->persist($entity);
                $em->flush();
                
                $redirectUrl = $this->generateUrl('nonchan_crud_list', array('name' => $name));
                
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    sprintf('%s created',  $definition->getName())
                );
                
                return $this->redirect($redirectUrl);
                
            }
            
        }
        
        $response = new Response();
        $response->setPrivate();
        
        $context = array(
            'definition' => $definition,
            'form' => $form->createView()
        );
        
        return $this->render('NonchanCoreBundle:CRUD:create.html.twig', $context, $response);
    }
}
