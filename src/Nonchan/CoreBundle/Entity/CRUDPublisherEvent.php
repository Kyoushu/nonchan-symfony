<?php

namespace Nonchan\CoreBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Serializer;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;

/**
 * Board
 *
 * @ORM\Table
 * @ORM\Entity
 * @JMS\ExclusionPolicy("all")
 * 
 */
class CRUDPublisherEvent{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \DateTime
     * 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     * 
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     */
    protected $created;
    
    /**
     * @ORM\Column(name="eventJson", type="text")
     */
    private $eventJson;
    
    /**
     *
     * @var \Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent
     * 
     * @JMS\Expose
     * @JMS\SerializedName("event")
     * @JMS\Type("Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     */
    private $event;
    
    /**
     * Get id
     * @return integer
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * Get created
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }
    
    /**
     * Set created
     * @param \DateTime $created
     * @return \Nonchan\CoreBundle\Entity\CRUDPublisherEvent
     */
    public function setCreated(\DateTime $created){
        $this->created = $created;
        return $this;
    }
    
    /**
     * Set eventJson
     * @param string $eventJson
     * @return \Nonchan\CoreBundle\Entity\CRUDPublisherEvent
     */
    public function setEventJson($eventJson){
        $this->eventJson = $eventJson;
        return $this;
    }
    
    /**
     * Get eventJson
     * @return string
     */
    public function getEventJson(){
        return $this->eventJson;
    }
    
    /**
     * Get event
     * @return \Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent
     */
    public function getEvent(){
        return $this->event;
    }
    
    /**
     * Set event
     * @param \Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent $event
     * @return \Nonchan\CoreBundle\Entity\CRUDPublisherEvent
     */
    public function setEvent(PublishEvent $event){
        $this->event = $event;
        return $this;
    }
    
    public function serializeEvent(Serializer $serializer, SerializationContext $usedContext){
        
        $groups = $usedContext->attributes->get('groups')->getIterator()[0];
        
        $entityContext = new SerializationContext();
        $entityContext->setGroups( $groups );
        
        $this->event->serializeEntity($serializer, $entityContext);
        
        $eventJsonContext = new SerializationContext();
        $eventJsonContext->setGroups( $groups );
        
        $this->eventJson = $serializer->serialize($this->event, 'json', $eventJsonContext);
        
    }
    
    public function deserializeEvent(Serializer $serializer, DeserializationContext $usedContext){
        
        $groups = $usedContext->attributes->get('groups')->getIterator()[0];
        
        $eventJsonContext = new DeserializationContext();
        $eventJsonContext->setGroups( $groups );
        
        $this->event = $serializer->deserialize($this->eventJson, 'Nonchan\CoreBundle\CRUD\Publisher\Event\PublishEvent', 'json', $eventJsonContext);
        
        $entityContext = new DeserializationContext();
        $entityContext->setGroups( $groups );
        
        $this->event->deserializeEntity($serializer, $entityContext);
        
    }
    
}

