<?php

namespace Nonchan\CoreBundle\Entity;

use Nonchan\CoreBundle\Model\NamedContentBase;
use Nonchan\CoreBundle\Table\Mapping as Table;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Nonchan\CoreBundle\Form\Mapping as Form;

/**
 * Post
 *
 * @ORM\Table()
 * @ORM\Entity
 * @JMS\ExclusionPolicy("all")
 */
class Post extends NamedContentBase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     * 
     * @Table\Column(label="ID")
     */
    private $id;
    
    /**
     *
     * @var string 
     * @ORM\Column(name="messageMarkdown", type="text")
     * 
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"crud.publisher", "crud.webservice"})
     * 
     * @Form\Field(
     *      type="textarea",
     *      options={"label"="Message"}
     * )
     */
    private $messageMarkdown;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get messageMarkdown
     * @return string
     */
    public function getMessageMarkdown(){
        return $this->messageMarkdown;
    }
    
    /**
     * Set messageMarkdown
     * @param string $messageMarkdown
     * @return \Nonchan\CoreBundle\Entity\Post
     */
    public function setMessageMarkdown($messageMarkdown){
        $this->messageMarkdown = $messageMarkdown;
        return $this;
    }
}
