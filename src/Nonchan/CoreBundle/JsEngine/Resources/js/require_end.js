function require(dependencies, callback){
    return __require('__global', dependencies, callback);
}

function define(moduleName, dependencies, callback){
    require(dependencies, function(){
        __requireLibs['__global'][moduleName] = callback.apply(callback, arguments);
    });
}

function __require(libName, dependencies, callback){
    
    if(typeof dependencies === 'string'){
        dependencies = [dependencies];
    }
    
    var resolvedDependencies = [];
    
    for(var index in dependencies){
        (function(){
            
            var dependency = dependencies[index];
            
            if(typeof __requireLibs["__global"][dependency] !== 'undefined'){
                resolvedDependencies.push(__requireLibs["__global"][dependency]);
                return;
            }
            else if(typeof __requireLibs[libName][dependency] !== 'undefined'){
                resolvedDependencies.push(__requireLibs[libName][dependency]);
                return;
            }
            else{
                throw('Could not find the module "' + dependency + '" within the library context "' + libName + '"');
            }
            
        })();
    }
    
    if(typeof callback === 'function'){
        callback.apply(callback, resolvedDependencies);
    }
    
    if(resolvedDependencies.length === 1) return resolvedDependencies[0];
    
}