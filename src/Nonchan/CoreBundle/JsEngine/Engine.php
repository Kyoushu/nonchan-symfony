<?php

namespace Nonchan\CoreBundle\JsEngine;

class Engine{
    
    private $v8js;
    
    public function __construct($objectName, array $variables = array(), array $dependencies = array()){
        $this->v8js = new \V8Js($objectName, $variables, $dependencies, true);
    }
    
    /**
     * Executes JS and returns print() output
     * 
     * @param string $script
     * @param string $identifier
     * @param integer $flags
     * @return string Output from print() calls in script
     * @throws \V8JsException
     */
    public function executeScript($script){
        
        try{
            ob_start();
            $this->v8js->executeString($script);
            $output = ob_get_contents();
            ob_end_clean();
            return $output;
        }
        catch (\V8JsException $e) {
            ob_end_clean();
            throw $e;
        }
        
    }
    
}