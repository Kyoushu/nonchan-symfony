<?php

namespace Nonchan\CoreBundle\JsEngine\Compiler;

use Symfony\Component\Finder\Finder;

class RequireJsScript {
    
    const REGEX_REQUIRE_CALL = '/require\(/';
    
    private $libs;
    
    public function __construct(){
        $this->libs = array();
    }
    
    public function registerLibrary($libName, $libPath){
        
        if(!file_exists($libPath)){
            throw new \RuntimeException(sprintf('The directory %s does not exist', $libPath));
        }
        
        $libPath = realpath($libPath);
        
        $this->libs[$libName] = array(
            'path' => $libPath,
            'includes' => array()
        );
        
        $rootPathRegex = sprintf('/^%s\//', preg_quote($libPath, '/'));
        
        $finder = new Finder();
        $finder->files()->in($libPath)->name('*.js')->exclude(array('test'));
        
        foreach($finder as $file){
            
            $includeName = preg_replace('/\.js$/', '', preg_replace($rootPathRegex, '', $file));
            
            $this->libs[$libName]['includes'][$includeName] = array(
                'path' => (string)$file
            );
            
        }
        
    }
    
    static function replaceRequireCalls($script, $libNameVarname){
        
        $script = preg_replace_callback(self::REGEX_REQUIRE_CALL, function($match) use ($libNameVarname){
            return sprintf('__%s%s, ', $match[0], $libNameVarname);
        }, $script);
        
        return $script;
        
    }
    
    static function includeLibJsonFiles(array &$script, $libName, $libPath, $libsVarName){
        
        $parentJsonFinder = new Finder();
        $parentJsonFinder->files()
            ->in(sprintf('%s/..', $libPath))
            ->name('*.json')
            ->depth(0);


        foreach($parentJsonFinder as $jsonFile){
            $jsonData = json_decode(file_get_contents($jsonFile));
            $script[] = sprintf(
                '    %s[%s][%s] = %s;',
                $libsVarName,
                json_encode($libName),
                json_encode('../' . $jsonFile->getFilename()),
                json_encode($jsonData)
            );
        }
        
        $jsonFinder = new Finder();
        $jsonFinder->files()
            ->in($libPath)
            ->name('*.json')
            ->depth(0);

        foreach($jsonFinder as $jsonFile){
            $jsonData = json_decode(file_get_contents($jsonFile));
            $script[] = sprintf(
                '    %s[%s][%s] = %s;',
                $libsVarName,
                json_encode($libName),
                json_encode($jsonFile->getFilename()),
                json_encode($jsonData)
            );

            $script[] = sprintf(
                '    %s[%s][%s] = %s[%s][%s];',
                $libsVarName,
                json_encode($libName),
                json_encode('./' . $jsonFile->getFilename()),
                $libsVarName,
                json_encode($libName),
                json_encode($jsonFile->getFilename())
            );
        }
    }
    
    private function generateScript(){
        
        $libsVarName = '__requireLibs';
        $libNameVarname = '__libName';
        
        $script = array();
        
        // Create global include container
        $script[] = sprintf('var %s = {"__global": {}};', $libsVarName);
        
        foreach($this->libs as $libName => $lib){
            
            $script[] = '';
            
            // Open libName variable wrapper
            $script[] = '(function(){';
            $script[] = sprintf('    var %s = %s;', $libNameVarname, json_encode($libName));
            
            // libName comment
            $script[] = '';
            $script[] = sprintf('    // Library: %s', $libName);
            
            $libPath = $lib['path'];
            
            // Create include object
            $script[] = sprintf('    %s[%s] = {};', $libsVarName, json_encode($libName));
            
            self::includeLibJsonFiles($script, $libName, $libPath, $libsVarName);
                        
            foreach($lib['includes'] as $includeName => $include){
                
                $script[] = '';
                
                // Open module closure
                $script[] = '    (function(){';
                $script[] = '';
                
                $includePath = $include['path'];
                
                // Search/replace references to require() and update to include __libName
                $includeScript = file_get_contents($includePath);
                $modifiedIncludeScript = self::replaceRequireCalls($includeScript, $libNameVarname);
                
                $script[] = '        var exports = {};';
                $script[] = '        var module = (function(){';
                $script[] = '            ' . str_replace("\n", "\n            ", $modifiedIncludeScript);
                $script[] = '        })();';
                
                $script[] = '';
                
                $script[] = sprintf('        if(typeof %s["__global"][%s] === "undefined"){', $libsVarName, json_encode($includeName));
                $script[] = sprintf('            %s["__global"][%s] = (module ? module : exports);', $libsVarName, json_encode($includeName));
                $script[] = '        }';
                
                // Create aliases for global calls
                $script[] = '';
                $script[] = sprintf(
                    '        %s[%s][%s] = %s["__global"][%s];',

                    $libsVarName,
                    json_encode($libName),
                    json_encode($includeName),

                    $libsVarName,
                    json_encode($includeName)
                );
                
                // Create aliases for same-path require() calls
                $script[] = sprintf(
                    '        %s[%s][%s] = %s[%s][%s];',
                        
                    $libsVarName,
                    json_encode($libName),
                    json_encode('./' . $includeName),
                        
                    $libsVarName,
                    json_encode($libName),
                    json_encode($includeName)
                    
                );
                
                // Close module closure
                $script[] = '';
                $script[] = '    })();';
                
            }
            
            $script[] = '';
            
            // Close libName variable wrapper
            $script[] = '})();';
            
        }
        
        $script[] = '';
        $script[] = file_get_contents(sprintf('%s/../Resources/js/require_end.js', __DIR__));
        
        return implode("\n", $script);
        
    }

    public function __toString(){
        return $this->generateScript();
    }
    
}