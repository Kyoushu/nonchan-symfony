<?php

namespace Nonchan\CoreBundle\JsEngine;

use Nonchan\CoreBundle\JsEngine\Factory;

class Templating {
    
    private $factory;

    public function __construct(Factory $factory){
        $this->factory = $factory;
        
    }
    
    public function render($viewName, $context = array()){
        $engine = $this->factory->createEngine('templating', array('context' => $context), array('require'));
        $script = 'print("Hello World!");';
        return $engine->executeScript($script, 'templating');
    }
    
}
