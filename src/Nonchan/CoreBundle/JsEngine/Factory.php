<?php

namespace Nonchan\CoreBundle\JsEngine;

use Symfony\Component\Finder\Finder;

class Factory {
    
    private $scripts;
    
    public function __construct(){
        $this->scripts = array();
    }
    
    /**
     * Add an extension definition to the factory
     * @param string $name
     * @param string $path Absolute path to JS file
     * @param array $dependencies Names of required extensions
     * @param boolean $autoEnable
     */
    public function registerExtension($name, $path, array $dependencies = array(), $autoEnable = false){
        $script = file_get_contents($path);
        $this->scripts[$name] = $script;
        \V8js::registerExtension($name, $script, $dependencies, $autoEnable);
    }
    
    public function registerExtensionScript($name, $script, array $dependencies = array(), $autoEnable = false){
        $this->scripts[$name] = $script;
        \V8js::registerExtension($name, (string)$script, $dependencies, $autoEnable);
    }
    
    public function registerExtensionDir($name, $path, array $dependencies = array(), $autoEnable = false){
        
        $finder = new Finder();
        $finder->files()->in($path)->name('*.js');
        
        $scriptParts = array();
        foreach($finder as $file){
            $scriptParts[] = sprintf('// %s', $file);
            $scriptParts[] = file_get_contents($file);
        }
        
        $script = implode("\n", $scriptParts);
        
        $this->scripts[$name] = $script;
        
        \V8js::registerExtension($name, $script, $dependencies, $autoEnable);
        
    }
    
    public function getScript($name){
        $script = &$this->scripts[$name];
        if(!isset($script)){
            throw new \RuntimeException(sprintf('Undefined extension %s', $name));
        }
        return (string)$script;
    }
    
    /**
     * 
     * @param string $objectName
     * @param array $variables
     * @return \Nonchan\CoreBundle\JsEngine\Engine
     */
    public function createEngine($objectName, array $variables = array(), array $dependencies = array()){
        return new Engine($objectName, $variables, $dependencies);
    }
}
