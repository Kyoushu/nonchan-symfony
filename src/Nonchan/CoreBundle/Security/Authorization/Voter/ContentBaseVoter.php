<?php

namespace Nonchan\CoreBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Exception\InvalidArgumentException;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ContentBaseVoter implements VoterInterface{
    
    const READ = 'read';
    const UPDATE = 'update';
    const DELETE = 'delete';
    
    public function supportsAttribute($attribute){
        
        return in_array($attribute, array(
            self::READ,
            self::UPDATE,
            self::DELETE
        ));
        
    }
    
    public function supportsClass($class){
        
        $supportedClass = 'Nonchan\CoreBundle\Model\ContentBase';
        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
        
    }
    
    public function vote(TokenInterface $token, $entity, array $attributes) {
        
        // Check if the token contains a user
        $user = $token->getUser();
        if( !$user instanceof UserInterface ) {
            return VoterInterface::ACCESS_DENIED;
        }
        
        // Allow admin access
        if(in_array('ROLE_ADMIN', $user->getRoles())){
            return VoterInterface::ACCESS_GRANTED;
        }
        
        // Check if we're accessing a ContentBase entity
        if( !$this->supportsClass(get_class($entity)) ){
            return VoterInterface::ACCESS_ABSTAIN;
        }
        
        if( count($attributes) !== 1 ) {
            throw new InvalidArgumentException(
                'Only one attribute is allowed, READ, UPDATE or DELETE'
            );
        }
        
        $attribute = strtolower($attributes[0]);
        if(!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }
        
        // If the entity is owned by the user, allow access
        if($entity->getAuthor()){
            if( $entity->getAuthor()->getId() === $user->getId() ){
                return VoterInterface::ACCESS_GRANTED;
            }
        }
        
        // Deny access if private
        if($entity->isPrivate()){
            return VoterInterface::ACCESS_DENIED;
        }
        
        if($attribute === self::READ) return VoterInterface::ACCESS_GRANTED;
        elseif($attribute === self::UPDATE) return VoterInterface::ACCESS_DENIED;
        elseif($attribute === self::DELETE) return VoterInterface::ACCESS_DENIED;
        
    }
    
}